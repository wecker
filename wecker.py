#!/usr/bin/env python2.5
import gtk
import hildon
import gobject
import pango

import osso
import time
import os
from pyalarmd import *

import ConfigParser
import sys


K_ESCAPE = 65307

class Configfile:
  def __init__(self):
    self.Filename = "/etc/wecker.conf"
    self.mp3 = ""
   
    self.cfg = ConfigParser.ConfigParser()
    self.Read()

  def Create(self):
    self.cfg.add_section("main")
    self.cfg.set('main', 'mp3', self.mp3)

  def Read(self):
    self.cfg.read(self.Filename)
    self.mp3 = self.cfg.get('main','mp3')

  def Write(self):
    self.cfg.set('main', 'mp3', self.mp3)
    fp = open (self.Filename, 'w' )
    self.cfg.write(fp)
    fp.close()

class Clock(hildon.Program):
  def __init__(self):
    hildon.Program.__init__(self)
    self.CreateMainWindow()
    self.HandleFullscreen()
    self.MakeMenu()
    self.StartClock()

    self.Wecker = Wecker()
    self.cfg = Configfile()
    self.Wecker.mp3 = self.cfg.mp3

  def CreateMainWindow(self):
    self.window = hildon.Window()
    self.window.connect("destroy", gtk.main_quit)
    self.window.set_title("Wecker")
    self.window.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))
    self.add_window(self.window)
 
  def HandleFullscreen(self):
    self.window.connect("key-press-event", self.on_key_press)
    self.window.connect("window-state-event", self.on_window_state_change)
    self.window_in_fullscreen = True
    self.window.fullscreen ()
 
  def on_window_state_change(self, widget, event, *args):
    if event.new_window_state & gtk.gdk.WINDOW_STATE_FULLSCREEN:
      self.window_in_fullscreen = True
    else:
      self.window_in_fullscreen = False

  def on_key_press(self, widget, event, *args):
    if event.keyval == gtk.keysyms.F6:
      if self.window_in_fullscreen:
        self.window.unfullscreen ()
      else:
        self.window.fullscreen ()
    elif event.keyval == K_ESCAPE:
      self.Wecker.DeleteRecentAlarms()

  def run(self):
    self.window.show_all()
    gtk.main()

  def StartClock(self):
    self.hello = gtk.Label("Wecker")
    self.window.add(self.hello)
    self.hello.modify_font(pango.FontDescription("sans bold 190"))
    self.hello.modify_fg(gtk.STATE_NORMAL, gtk.gdk.color_parse("blue"))
    self.timer = gobject.timeout_add (1000, self.AdvanceClock)

  def AdvanceClock(self):
    self.localtime = time.localtime()
    self.hello.set_text("%2d"%self.localtime[3]+":%2.2d"%self.localtime[4])
    return True # for fuckin timer

  def MakeMenu(self):
    self.MakeTopMenu()
    self.MakeScreenMenu()

  def MakeScreenMenu(self):
    return
    #self.hello.connect_object("clicked", self.NewAlarm, self.window)

  def MakeTopMenu(self):
    self.menu = gtk.Menu()
    self.window.set_menu(self.menu)

    NA = gtk.MenuItem("New alarm")
    self.menu.append(NA)
    NA.connect_object("activate", self.NewAlarm, self.window)
    NA.show()

    SF = gtk.MenuItem("Select file")
    self.menu.append(SF)
    SF.connect_object("activate", self.PickFile, self.window)
    SF.show()

    DRA = gtk.MenuItem("Delete recent alarms")
    self.menu.append(DRA)
    DRA.connect_object("activate", self.DeleteAlarms, None)
    DRA.show()


    
  def NewAlarm(self, widget):
    self.PickDate(self.window)
    self.PickTime(self.window)
    self.Wecker.AlarmTime=[self.AlarmDate[0],self.AlarmDate[1],self.AlarmDate[2],self.AlarmTime[0]-1,self.AlarmTime[1],0,0,0,0]
    dlg = hildon.Note ("information", (self.window, "Alarm @ "+str(self.Wecker.AlarmTime), self.Wecker.ic) )
    dlg.set_button_text("Hmkeh...")
    dlg.run()
    dlg.destroy()
    self.Wecker.SetAlarm()

  def DeleteAlarms(self, widget):
    self.Wecker.DeleteRecentAlarms()
    dlg = hildon.Note ("information", (self.window, "Deleting Alarms ", self.Wecker.ic) )
    dlg.set_button_text("Hmkeh...")
    dlg.run()
    dlg.destroy()

  def PickFile(self, widget):
    dlg = hildon.FileChooserDialog(self.window, gtk.FILE_CHOOSER_ACTION_OPEN)
    res = dlg.run()
    if res == gtk.RESPONSE_OK:
      self.Wecker.mp3 = dlg.get_filename()
      self.cfg.mp3 = self.Wecker.mp3
      self.cfg.Write()
    dlg.destroy()

  def PickDate(self, widget):
    CurrentDate = time.localtime()
    dlg = hildon.CalendarPopup (self.window, CurrentDate[0], CurrentDate[1], CurrentDate[2])
    dlg.run()
    self.AlarmDate = dlg.get_date()
    dlg.destroy()

  def PickTime(self, widget):
    CurrentDate = time.localtime()
    dlg = hildon.TimePicker(self.window)
    dlg.set_time(CurrentDate[3], CurrentDate[4])
    dlg.run()
    self.AlarmTime = dlg.get_time()
    dlg.destroy()


class Wecker:
   def __init__(self):
      self.mp3=""
      self.ff=568+ALARM_EVENT_NO_DIALOG
      self.ic="/usr/share/pixmaps/wecker_icon_26x26.png"
      self.msg="Hui Buh"
      self.prog="/usr/bin/wecker_alarm.sh "
#      self.prog="/usr/bin/mplayer -loop 0 "
#      self.prog="ps x | awk '/\/usr\/bin\/wecker/{exit 1}' && /usr/bin/wecker ; /usr/bin/mplayer -loop 0 "

   def DeleteRecentAlarms(self):
      self.CurrentTime = int((time.mktime(time.localtime())+1))
      self.MagicMinTime = 0
      for self.event in alarm_query(self.MagicMinTime, self.CurrentTime):
         cancel_alarm (self.event)
      os.popen("killall -KILL mplayer")

   def DeleteFutureAlarms(self):
      self.CurrentTime = int(time.mktime(time.localtime()))
      self.MagicMaxTime = 9999661908
      for self.event in alarm_query(self.CurrentTime, self.MagicMaxTime):
         cancel_alarm (self.event)

   def SetAlarm(self):
      self.CurrentTime = int(time.mktime(time.localtime()))
      cmd=self.prog+"\""+self.mp3+"\""
      add_alarm(int(time.mktime(self.AlarmTime)), dbus_name='alarm_responded', flags= self.ff, message=self.msg, exec_name=cmd, icon=self.ic)

app = Clock()
app.run()
