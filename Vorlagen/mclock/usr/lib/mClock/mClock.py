#!/usr/bin/python2.5
# -*- coding: latin-1 -*-

import pygame
from pygame.locals import *

import os
#Set as early as wanted, will make sure that the "Loading..." banner disappers
#Only needed because this is a pure pygame app right now, wouldn't be needed when using GTK!
os.environ["SDL_VIDEO_X11_WMCLASS"]="mClock"

import urllib
import time
import datetime
import thread
import math
import random
import locale

import dbus.glib
#import hildon

import osso
import gobject
import gtk

import sys
from gnome import gconf 

from sun import *

EVT_STEP = pygame.USEREVENT
EVT_STEPSCREEN = pygame.USEREVENT+1
EVT_STEPMAP = pygame.USEREVENT+2

sys.path.append('/usr/lib/mClock/')
path = '/usr/share/mClock/img'

bFullscreen = True
bTimerRunning = False
backBottom = pygame.Surface((800, 80))
currentMap = pygame.Surface((0, 0))

maps = ["1-Winter-January.jpg", "2-Spring-April.jpg", "3-Summer-July.jpg", "4-Fall-October.jpg"]
currentMapIndex = 2
time.localtime()[7] #Day of year, 1-366
#From http://www.astro.uu.nl/~strous/AA/en/antwoorden/seizoenen.html
#timedelta(  	[days[, seconds[, microseconds[, milliseconds[, minutes[, hours[, weeks]]]]]]])
timeDeltaSpring = datetime.timedelta(365,  2, 0, 0, 49, 5, 0)
timeDeltaSummer = datetime.timedelta(365, 57, 0, 0, 47, 5, 0)
timeDeltaAutumn = datetime.timedelta(365, 31, 0, 0, 48, 5, 0)
timeDeltaWinter = datetime.timedelta(365, 33, 0, 0, 49, 5, 0)

startSpring2005 = datetime.datetime(2005,  3, 20, 11, 33, 19)  #20 March 2005	11:33:19
startSummer2005 = datetime.datetime(2005,  6, 21,  6, 39, 11)  # 21 June 2005 06:39:11
startAutumn2005 = datetime.datetime(2005,  9, 22, 22, 16, 34)  #22 September 2005 22:16:34
startWinter2005 = datetime.datetime(2005, 12, 21, 18, 34, 51)  #21 December 2005 18:34:51

yearsSince = time.localtime()[0] - 2005

startSpring = startSpring2005+yearsSince*timeDeltaSpring
startSummer = startSummer2005+yearsSince*timeDeltaSummer
startAutum = startAutumn2005+yearsSince*timeDeltaAutumn
startWinter = startWinter2005+yearsSince*timeDeltaWinter

now = datetime.datetime.utcnow()
if(now<startSpring or now>startWinter):
  currentMapIndex=0
elif(now>startSpring and now<startSummer):
  currentMapIndex=1
elif(now>startSummer and now<startAutum):
  currentMapIndex=2
elif(now>startAutum and now<startWinter):
  currentMapIndex=3
print "Season = "+maps[currentMapIndex].split("-")[1]

#################################################
# Class that handles all display stuff
#################################################
class display:
 def __init__(self):
  pygame.init()
  pygame.mixer.quit() # we don't want pygame hogging the audio device (from KAGU, don't know if it matters here)
   #disable the cursor
  pygame.mouse.set_visible(False)
  pygame.display.set_caption("mClock")

  self.scale = False
  self.screen = pygame.display.set_mode((800,480),FULLSCREEN,16)
  self.back = pygame.image.load(os.path.join(path,"0-Night.jpg")).convert()
  # global powerIcon
  # powerIcon = pygame.image.load(os.path.join(path,"power.png")).convert_alpha() 
  global sunIcon
  sunIcon = pygame.image.load(os.path.join(path,"sun.png")).convert_alpha() 
  
#################################################
 def showMap(self, initialRun):
  theSun = Sun()
  global currentMapIndex
  load = pygame.image.load(os.path.join(path,maps[currentMapIndex])).convert()
  font = pygame.font.Font(None, 9)
  blue = Color( "#202080" )
  yellow = Color( "Yellow" )
  colorMask = Color( "Pink" )
  now = datetime.datetime.utcnow()
  year = now.year
  month = now.month
  day = now.day
  hours = now.hour
  minutes = now.minute
  seconds = now.second
  timeInMinutes = (now.hour)*60.0+(1.0/60.0*now.minute)

  height = load.get_height()
  width = load.get_width()
  step = 10;
  pygame.draw.line(load, blue, (0,200), (800,200), 1)

  N = theSun.daysSince2000Jan0(year, month, day)
  res =  theSun.sunRADec(N)
  declination = res[1]
  dec=declination
  if(dec==0.0):
      dec=-0.001
  midday = datetime.datetime(year, month, day, 12, 00, 00)
  GMST0 = theSun.GMST0(theSun.daysSince2000Jan0(year,month,day))
  valueTau = -10.0
  tau=0.0 # Hour Angle of the Sun
  tau=GMST0

  STD = 1.0*(hours) + minutes/60.0 + seconds/3600.0
  GHA = theSun.computeGHA(day, month, year, STD);

  lon=0    #For UT/GMT
  d = theSun.daysSince2000Jan0(year,month,day)
  # Compute local sidereal time of this moment
  sidtime = theSun.GMST0(d)
  # Compute Sun's RA + Decl at this moment
  res = theSun.sunRADec(d)

  sRA = res[0]
  sdec = res[1]
  sr = res[2]
  # Compute time when Sun is at south - in hours UT
  tsouth = 12.0 - theSun.rev180(sidtime - sRA)/15.0;
  tau=GHA
  K = math.pi/180.0
  y0=90
  x0=180    #the center of the map is at x0, y0

  #setup some debugtext
  debugText = maps[currentMapIndex].split("-")[1]
  bPosDec = True
  if(dec < 0):
      bPosDec = False

  for i in range(-180,180,1):
    longitude=i+tau;
    tanLat = - math.cos(longitude*K)/math.tan(dec*K)
    arctanLat = math.atan(tanLat)/K
    y1 = y0 - int(arctanLat+0.5)    #int
    longitude=longitude+1
    tanLat = - math.cos(longitude*K)/math.tan(dec*K)
    arctanLat = math.atan(tanLat)/K
    y2 = y0 - int(arctanLat+0.5)
    pygame.draw.line(load, blue, (2.22*(x0+i),2.22*y1), (2.22*(x0+i+1),2.22*y2), 3)
    if(bPosDec):
      pygame.draw.line(load, colorMask, (2.22*(x0+i),2.22*y1), (2.22*(x0+i),400), 3)
    else:
      pygame.draw.line(load, colorMask, (2.22*(x0+i),2.22*y1), (2.22*(x0+i),0), 3)

  #Draw the sun
  # yellow = Color( "Yellow" )
  # gold = Color( "Gold" )
  x = 400
  if(tau<180.0):
    x = 400.0-int(800.0/360.0*tau)
  else:
    x = 1200-int(800.0/360.0*tau)
  # pygame.draw.circle(load, gold, (int(x),200-int(400/180*dec)), 8, 0)
  # pygame.draw.circle(load, yellow, (int(x),200-int(400/180*dec)), 4, 0) 
  load.blit(sunIcon, (int(x+8),200+8-int(400/180*dec)))
  
  load.set_colorkey(colorMask, pygame.RLEACCEL)
  self.screen.blit(self.back,(0,0))
  self.screen.blit(load,(0,0))
  self.screen.blit(backBottom, (0,400))
  
  #Draw power icon
  # if(alwaysOn == True):
    # self.screen.blit(powerIcon, (387,425)) 
  
  global currentMap
  currentMap = self.screen.copy()
    
  font = pygame.font.Font(None, 26)
  debugInfo = font.render(debugText, 1, (128, 128, 255, 192))
  self.screen.blit(debugInfo, (0,self.screen.get_height()-debugInfo.get_height()))
  if(initialRun==True):
    pygame.display.flip()
    
#################################################  
 def UpdateTime(self, displayMode):
  #Reset bottom to black
  global currentMap
  if(displayMode=="maxi"):
    self.screen.blit(currentMap, (0,0))
  self.screen.blit(backBottom, (0,400))

  #Update date
  dateFont = pygame.font.Font(None, 96) 
  theDate = datetime.datetime.now().strftime("%a %d %b")  
  #latin-1 needed to properly render s�b and such...
  theEncoding = 'Latin-1'
  textDate = dateFont.render(theDate.encode(theEncoding), 1, (128, 128, 255, 192)) 

  self.screen.blit(textDate, (self.screen.get_width()-textDate.get_width(),self.screen.get_height()-textDate.get_height()))
  dateRect = pygame.Rect(self.screen.get_width()-textDate.get_width(),self.screen.get_height()-textDate.get_height(),textDate.get_width(),textDate.get_height())
  #Update font
  if(displayMode=="maxi"):
    theTime = datetime.datetime.now().strftime("%H:%M")
    timeFont = pygame.font.Font(None, 384)
    textTime = timeFont.render(theTime, 1, (192, 192, 255, 192))
    self.screen.blit(textTime, ((self.screen.get_width()-textTime.get_width())/2,(400-textTime.get_height())/2))
    timeRect = pygame.Rect((self.screen.get_width()-textTime.get_width())/2,(400-textTime.get_height())/2,textTime.get_width()+20,textTime.get_height())
  else: #displaymode="mini"
    theTime = datetime.datetime.now().strftime("%X")
    timeFont = pygame.font.Font(None, 96)
    textTime = timeFont.render(theTime, 1, (128, 128, 255, 192))
    self.screen.blit(textTime, (0,self.screen.get_height()-textTime.get_height()))
    timeRect = pygame.Rect(0,self.screen.get_height()-textTime.get_height(),textTime.get_width()+20,textTime.get_height())
  #Draw power icon
  # if(alwaysOn == True):
    # self.screen.blit(powerIcon, (387,425))
    # if(displayMode=="maxi"):
      # device.display_blanking_pause()
  #Update display
  pygame.display.update((dateRect,timeRect))

#Display
#################################################

#################################################
def keepDisplayOn():
 global device
 device.display_blanking_pause()
 print "keepDisplayOn() at " + datetime.datetime.now().strftime("%X")

#################################################
def state_cb(shutdown, save_unsaved_data, memory_low, system_inactivity, message, loop):
 global calls
 print "Shutdown: ", shutdown
 print "Save unsaved data: ", save_unsaved_data
 print "Memory low: ", memory_low
 print "System Inactivity: ", system_inactivity
 print "Message: ", message
 return False

#################################################
def quit():
 global timeMode
 conf_client = gconf.client_get_default()
 conf_client.add_dir("/apps/mClock/general", gconf.CLIENT_PRELOAD_NONE)
 conf_client.set_string("/apps/mClock/general/timeMode", timeMode)
 global osso_c
 osso_c.close()
 #device.set_device_state_callback(None)
 exit()

################################################# 
def switchMode():
  global timeMode, maxiAndFirstRun
  if(timeMode=="maxi"):
    timeMode="mini"
    maxiAndFirstRun = False
    pygame.time.set_timer(EVT_STEP, 1000)      
  else:
    timeMode="maxi"
    maxiAndFirstRun = True
    secondsRemainingInMinute = 62-time.localtime()[5]
    pygame.time.set_timer(EVT_STEP, secondsRemainingInMinute*1000)  #Call the update right after the minute change (and from then on every minute)      
  d.showMap(True)
  d.UpdateTime(timeMode)
 
################################################# 
def main():
 global loop, osso_c, device
 loop = gobject.MainLoop()
 osso_c = osso.Context("mclock", "1.0.0", False)
 device = osso.DeviceState(osso_c)
 
 global timeMode
 timeMode = "maxi"
 global maxiAndFirstRun
 maxiAndFirstRun = False
 global currentMapIndex
 
 # global alwaysOn
 # alwaysOn = False
 
 #load timeMode value from gconf
 conf_client = gconf.client_get_default()
 conf_client.add_dir("/apps/mClock/general", gconf.CLIENT_PRELOAD_NONE)
 timeModeStored = conf_client.get_string("/apps/mClock/general/timeMode")
 if(timeModeStored!="maxi" and timeModeStored!="mini"):
  conf_client.set_string("/apps/mClock/general/timeMode", "maxi")
  timeModeStored = "maxi"
 timeMode = timeModeStored
 
 device.display_state_on()  #turn the display on (handy when debugging and screen turned off while editing from remote
 
 global d
 d = display()
 d.showMap(True)
 d.UpdateTime(timeMode)
 #start the timer to update the time display once per second or once per minute
 pygame.time.set_timer(EVT_STEP, 1000 if timeModeStored == "mini" else 60000)
 pygame.time.set_timer(EVT_STEPMAP, 108000) #Map needs update every ~2 minutes 
 
 global bFullscreen
 # Wait for user input
 while True:
  pygame.time.wait(50)  #trying to keep a low CPU usage, but unsure if it helps in any way, as the next one should already be enough
  event = pygame.event.wait() #While the program is waiting it will sleep in an idle state
  if (event.type == KEYUP):
    if (event.key == 287):			    # Fullscreen button / F6 : Toggle fullscreen
      pygame.display.toggle_fullscreen()
      bFullscreen = ~bFullscreen  #invert
      if not(bFullscreen):
        timeMode="maxi"
        switchMode()
    if (event.key != K_ESCAPE):		     # K_ESCAPEEsc/Back button -> Exit
      #quit()
      if (event.key == 13):              # 13, DPad-center ->
        switchMode()
      if (event.key == 275):			 # 275, DPad-right ->
        currentMapIndex = currentMapIndex+1
        if(currentMapIndex>3):
          currentMapIndex = 0
        print currentMapIndex
        d.showMap(True)
        d.UpdateTime(timeMode)
      if (event.key == 276):             # 276, DPad-left ->
        currentMapIndex = currentMapIndex-1
        if(currentMapIndex<0):
          currentMapIndex = 3
        print currentMapIndex
        d.showMap(True)
        d.UpdateTime(timeMode)
      # if (event.key == 273):             # 273, DPad-up ->
        # if(alwaysOn == False):
          # alwaysOn = True
          # device.display_blanking_pause() 
          # pygame.time.set_timer(EVT_STEPSCREEN, 55000)
        # else:
          # alwaysOn = False
          # pygame.time.set_timer(EVT_STEPSCREEN, 0)
        # d.showMap(True)
        # d.UpdateTime(timeMode)
    #    if (event.key == 289):			    # Zoom out ->
    #    if (event.key == 288):			    # Zoom in ->
    else:
      quit()
  if (event.type == EVT_STEP):          #Timer-Event
    if(maxiAndFirstRun == True):
      maxiAndFirstRun = False
      pygame.time.set_timer(EVT_STEP, 60000) #Initial right-after-the-minute update called, now repeat once per minute
    d.UpdateTime(timeMode)
  if (event.type == EVT_STEPSCREEN):         #Timer-Event
    keepDisplayOn()
  if (event.type == EVT_STEPMAP):          	 #Timer-Event
     d.showMap(False)
  if (event.type == MOUSEBUTTONDOWN):        #Touchscreen-Event
    pos = pygame.mouse.get_pos()
	#touch-events. pos[0]=x, pos[1]=y
    if(pos[0]>=0 and pos[0]<200 and pos[1]>400 and pos[1] <480):  #bottom left
      switchMode()
    if(pos[0]>=0 and pos[0]<200 and pos[1]>160 and pos[1] <320):  #left: previous season
      currentMapIndex = currentMapIndex-1
      if(currentMapIndex<0):
        currentMapIndex = 3
      print currentMapIndex
      d.showMap(True)
      d.UpdateTime(timeMode)
    if(pos[0]>=600 and pos[0]<800 and pos[1]>160 and pos[1] <320):  #right: next season
      currentMapIndex = currentMapIndex+1
      if(currentMapIndex>3):
        currentMapIndex = 0
      print currentMapIndex
      d.showMap(True)
      d.UpdateTime(timeMode)  
#################################################      
if __name__ == '__main__':
 main()


