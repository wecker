import bdist_debian
from distutils.core import setup

setup(name='wecker',
      version='0.1',
      section='user/other',
      maintainer='Gerolf Ziegenhain',
      maintainer_email='Gerolf Ziegenhain <gerolf@ziegenhain.com>',
      depends='python2.5, python2.5-hildon, python2.5-gtk2, mplayer',
      description="Wecker",
      long_description="A simplistic alarm clock using mplayer.",
      scripts=['wecker', 'wecker.py', 'pyalarmd.py', 'wecker_alarm.sh'],
      data_files = [
         ('share/pixmaps',             ['wecker_icon_26x26.png']),
         ('share/applications/hildon', ['wecker.desktop']),
      ],
      icon='wecker_icon_26x26.png',
      cmdclass={'bdist_debian': bdist_debian.bdist_debian}
)
